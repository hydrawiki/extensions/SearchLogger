mw.loader.using('mediawiki.widgets.DateInputWidget').then(function() {
	var sdate = new mw.widgets.DateInputWidget({name:"start_date",inputId:"start_date",value:$("#start_date").val()});
	$("#start_date").replaceWith(sdate.$element);
	var edate = new mw.widgets.DateInputWidget({name:"end_date",inputId:"end_date",value:$("#end_date").val()});
	$("#end_date").replaceWith(edate.$element);
});